#! usr/bin/env python3
import os
FILENAME = 'co2_emission.csv'

(PAIS,
CODIGO,
 AGE,
 CO2) = range(4)

def opendata():
    temp = open(FILENAME)
    arch_dic = {}
    dic_cont = {}
    a = int(0)
    b = str

    for counter, linea in enumerate(temp):
        if counter != 0:
            temp_linea = linea.split(',')
            pais = temp_linea[PAIS].strip() 
            if pais != b:
                a = 0
            if arch_dic.get(pais):
                dic_cont[pais].update()
            else:
                a = a + 1
                b = pais
                dic_cont[pais] = a
    temp.close()
    return dic_cont
def registros_pais(dic_cont):
    regis_pais = ['Chile']
    a_value = int(0)
    b_value = int(286)
    contador = int(0)

    for key, value in dic_cont.items():
        a_value = value
        if a_value < b_value:
            b_value = a_value
            paisMenor = key
            regis_pais[contador] = paisMenor
        elif a_value == b_value:
            b_value = a_value
            paisMenor = key
            regis_pais.append(paisMenor)
            contador = contador + 1

    print(f'Los paises con más registros de CO2 son {regis_pais} con {b_value}.')
    
def main():
    dic_cont = opendata()
    registros_pais(dic_cont)
    
main()

resp = "si"
while resp != "no":
    main()
    resp = input("¿Desea continuar?\nSi desea continuar escriba 'si'\nSi no desea continuar escriba 'no'\nRespuesta: ")
    while resp != "si" and resp != "Si" and resp != "no" and resp != "No":
        resp = input("La opcion ingresada es invalida, verifique su respuesta.\nRespuesta: ")

