#!/usr/bin/env python3

FILENAME = "co2_emission.csv"

# dic = {"PAIS": {"CODIGO": CODE,
#                "ANIO": "EMISION",
#                "ANIO": "EMISION",
#                "ANIO": "EMISION",
#                "ANIO": "EMISION",
#                "ANIO": "EMISION",
#                "ANIO": "EMISION",
#                "ANIO": "EMISION",
#                "ANIO": "EMISION",
#                "ANIO": "EMISION"}}
#

(PAIS,
 CODIGO,
 ANIO,
 CO2) = range(4)


def openfile():
    temp = open(FILENAME)
    diccionario = {}

    for counter, linea in enumerate(temp):
        if counter != 0:
            temp_linea = linea.split(",")
            # print(temp_linea[CO2].strip())
            pais = temp_linea[PAIS].strip()
            codigo = temp_linea[CODIGO].strip()
            anio = temp_linea[ANIO].strip()
            co2 = temp_linea[CO2].strip()

            temp_dic = {"codigo": codigo,
                        anio: co2}

            if diccionario.get(pais):
                diccionario[pais].update(temp_dic)
            else:
                diccionario[pais] = temp_dic
    temp.close()

    return diccionario


def buscar_pais():
    pais = input("Ingrese un país: ")
    return pais.capitalize()


def muestra_data(data, pais):
    diccionario = data[pais]
    suma = 0
    for key, value in diccionario.items():
        # print(key, value)
        if key != "codigo":
            suma = suma + float(value)

    print(f"El total emitido por {pais} de toneladas de CO2 es {suma}")


def total_paises(data):
    total = len(data)
    print(f"El total de registros distintos(paises) es {total}")


def main():
    dic = openfile()
    # muestra_data(data=dic, pais="Chile")
    pais = buscar_pais()
    muestra_data(data=dic, pais=pais)
    total_paises(dic)

resp = "si"
while resp != "no" and resp != "No":
    main()
    resp = input("¿Desea continuar?\nEscriba 'si' para continuar\nEscriba 'no' para salir del programa\nRespuesta: ")
    while resp != "si" and resp != "Si" and resp != "no" and resp != "No":
        resp = input("Respuesta invalida, por favor verifique su respuesta\nRespuesta: ")

